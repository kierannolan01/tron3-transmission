/* 
 * File:        main.c
 * Author:      470414981
 * 
 * Description: Uses interrupts to blink an LED every 500ms, and blink two
 *              additional LEDs on respective button presses
 * 
 * Created on 21 September 2020, 6:41 PM

 *  */
#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include "ConfigRegsPIC18F452.h"

void setup(void);
void interruptSetup(void);
void highInterrupt(void);
void highPriorityISR(void);

void heartBeat(void);
void sendPing(void);
void respondPing(void);

void timerSetup(void);
void low_isr(void);
void low_vector(void);

// Data to transmit (DONT USE 11111111, 11110000, 00001111)
int data = 0b01010101;

// Flags whether data has been received
int dataReceivedFlag = 0;

void main(void){
    
    // Initial config
    setup();
    interruptSetup();
//    heartBeatSetup();
    timerSetup();
    
    // Infinite loop
    while(1){ 
        // Transmit data
            // Wait until transmit flag is 1 (TXREG empty)
            while(!PIR1bits.TXIF){ };
            TXREG = data;
    }
}

// Initial configuration of SFR's
void setup(void){ 
    
    // Configure LEDs as output
    PORTD = 0x00;
    TRISD = 0x00;   
    
    TRISC = 0b10000000;
    PORTC = 0x00;
    
    //TRISCbits.RC6 = 0; //Configure RC6 as output
    //TRISCbits.RC7 = 1; //Configure RC7 as input
    
    SPBRG = 0b01000000; // ={64}_10 Baud rate 9600 (p171 PIC18FXX2 Data Sheet)
    
    TXSTAbits.SYNC = 0; // Select USART asynchronous mode
    TXSTAbits.TXEN = 1; //Enable transmission
    TXSTAbits.BRGH = 1; //High speed baud select
       
    RCSTAbits.SPEN = 1; //Enable serial port
    RCSTAbits.CREN = 1; // Enable reception
  
//    PIE1bits.TXIE = 0; // Disable transmission interrupts
//    PIE1bits.RCIE = 0;  // Disable reception interrupts
}

void interruptSetup(void){
    RCONbits.IPEN = 1; //Enable priority mode
   
    INTCONbits.GIEL = 1; //Enable all high priority interrupts
//    INTCONbits.PEIE = 1; //Enable all unmasked peripheral interrupts
    
    PIE1bits.RCIE = 1; //Enable USART receive interrupt
    PIE1bits.TXIE = 0; //Disable USART transmit interrupt
    
    IPR1bits.RCIP = 1; //USART receive interrupt high priority
}

void timerSetup(void){
    
    INTCONbits.GIEL=1; // Enable LP interrupts
    
    // Generate an interrupt every 0.21s to recalculate motor speed (TMR1 OF)
    PIR1bits.TMR1IF=0;      // Clear TMR1 IF
    PIE1bits.TMR1IE=1;      // Enable TMR1 overflow interrupt
    IPR1bits.TMR1IP=0;      // TMR1 overflow low priority
    T1CONbits.RD16=1;       // RW 16 bit values
    T1CONbits.TMR1CS=0;     // Internal clock (2.5MHz)
    T1CONbits.T1CKPS0=1;    // 1:8 (2.5e6/8=312.5kHz)
    T1CONbits.T1CKPS1=1;    // 1:8 (TMR1 fill up every 0.21s)
    T1CONbits.TMR1ON=1;     // Enable Timer 1
}

// Place goto timer_ISR instruction at absolute address 0x08
#pragma code highPriorityInterruptAddress = 0x08
void highInterrupt(void){
    _asm goto highPriorityISR _endasm
}

// High priority ISR triggered by external interrupts
#pragma code
#pragma interrupt highPriorityISR
void highPriorityISR(void){
    
    // Receive data
    if(PIR1bits.RCIF == 1){
     
        //------------------------------
        // FUNCTION TO PROCESS RECEVIED DATA
        // Include respond to call function
        PORTD = RCREG;
        
        if(RCREG == 0b00001111){
            respondPing();
        }
        //------------------------------
        
        dataReceivedFlag = 1;
    }
    
//    // If timer overflowed
//    if(TIMEROVERFLOW == 1){
//        heartBeat();
//    }
}

// If timer overflowed
void heartBeat(void){
    
    // If no data received in the last 200ms (first time)
    if(dataReceivedFlag == 0){       
        dataReceivedFlag = -1;
        sendPing();
    }
    // If no data received in the last 2x200ms (including no ping response)
    else if(dataReceivedFlag == -1){
        // ERRORSTATE
        PORTD = 0b11111111;
        
        sendPing();
    }
    // If all is functioning as normal
    else{
        // Reset flag to 0
        dataReceivedFlag = 0;
    }
}

// Sends a ping - a call to the other xBee to verify correct operation
void sendPing(void){
    while(!PIR1bits.TXIF){ };
    TXREG = 0b00001111;           // (Temporary) Ping ID
}

// Responds to a ping - confirms correct xBee operation
void respondPing(void){
    while(!PIR1bits.TXIF){ };
    TXREG = 0b11110000;           // (Temporary) Response ID
}


#pragma code lowPriorityInterruptAddress=0x0018
void low_vector(void)
{
   _asm GOTO low_isr _endasm
}


#pragma interruptlow low_isr
void low_isr(void)
{   
    // Every 0.21 seconds recalculate the motor RPM
    if ( PIR1bits.TMR1IF )
    {   
        heartBeat();
    }
    
}

